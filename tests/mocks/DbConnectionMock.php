<?php


class DbConnectionMock implements DbConnectionInterface
{

    function execute(string $command)
    {
        if ($command === "SELECT `password` FROM `user` WHERE `email` = 'test@test.com';") {
            echo 'Dane prawidłowe! ';
            return ['password' => '$2y$11$.XuHMUBVwXVgIOF3.yOBLOJTTidFnZrNm7JrqZJHC8ne54MtkRAcC'];
        }
        echo 'Błedne dane! ';
        return false;
    }
}