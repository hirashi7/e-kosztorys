<?php

use PHPUnit\Framework\TestCase;

require_once '../core/App.php';

class IdentityServiceTest extends TestCase
{
    function testUserValidation_ValidDataGiven_ShouldReturnTrue()
    {

        // arrange
        $app = new App(new DbConnectionMock(), new IdentityService());
        $identity = $app->identity;

        // act
        $result = $identity->isUserValid('test@test.com', 'test');

        // assert
        self::assertTrue($result);
    }

    function testUserValidation_InvalidDataGiven_ShouldReturnFalse()
    {

        // arrange
        $app = new App(new DbConnectionMock(), new IdentityService());
        $identity = $app->identity;

        // act
        $result1 = $identity->isUserValid('test@test.com', 'badpassword');
        $result2 = $identity->isUserValid('test@...pl', 'test');
        $result3 = $identity->isUserValid('ashgashgajshjas', 'ashgashgajshjas');
        $result4 = $identity->isUserValid(true, true);
        $result5 = $identity->isUserValid(false, 0);
        $result6 = $identity->isUserValid('null', 'null');
        $result7 = $identity->isUserValid('', '');

        // assert
        self::assertFalse($result1);
        self::assertFalse($result2);
        self::assertFalse($result3);
        self::assertFalse($result4);
        self::assertFalse($result5);
        self::assertFalse($result6);
        self::assertFalse($result7);
    }

}