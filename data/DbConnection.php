<?php


class DbConnection implements DbConnectionInterface
{
    function execute(string $command)
    {
        $db = new mysqli('localhost', 'root', '', 'kosztorys');
        if ($db->connect_error != 0) {
            return false;
        }
        $query = $db->query($command);
        if ($query === false) {
            $db->close();
            return false;
        }

        return $query->fetch_assoc();
    }
}