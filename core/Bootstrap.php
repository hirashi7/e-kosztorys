<?php

class Bootstrap {

    /**
     * @var RouterInterface
     */
    private $_router;

    /**
     * @var App instance
     */
    private $_app;

    public function __construct(App $app, RouterInterface $router)
    {
        $this->_router = $router;
        $this->_app = $app;
        $this->_router->setAppInstance($this->_app);
        $this->_init();
    }

    /**
     * Handles routing
     */
    private function _init()
    {
        if(!isset($_SERVER['REDIRECT_URL'])) {
            $this->_router->redirectHome();
            return;
        }

        $request = explode('/',$_SERVER['REDIRECT_URL']);

        $request = array_filter($request, function($item) {
            if ($item !== "") {
                return true;
            }
            return false;
        });

        $request = array_values($request);

        switch (sizeof($request)){
            case 1:
                $this->_router->redirect(ucfirst($request[0]) . 'Controller', 'indexAction');
                break;
            case 2:
                $this->_router->redirect(ucfirst($request[0]) . 'Controller', $request[1] . 'Action');
                break;
            default:
                $this->_router->redirect(ucfirst($request[0]) . 'Controller', $request[1] . 'Action', $request[2]);
                break;

        }
    }
}

