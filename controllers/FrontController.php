<?php

use Core\Controller;

class FrontController extends Controller
{
    public function __construct($app)
    {
        parent::__construct($app);
        $this->name = 'front';
    }

    public function indexAction(){
        $this->render('index');
    }

    public function deleteAction(int $id = null){
        $this->render('delete', $id);
    }

    public function updateAction(int $id = null){
        $this->render('update', $id);
    }

}