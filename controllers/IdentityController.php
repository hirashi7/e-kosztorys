<?php

use Core\Controller;

class IdentityController extends Controller
{
    public function __construct($app)
    {
        parent::__construct($app);
        $this->name = 'identity';
    }

    public function loginAction()
    {
        $identity = $this->app->identity;

        if ($identity->isUserLoggedIn()) {
            header('location: /front/index');
            exit;
        }

        $form_submitted = false;
        $email = '';
        $password = '';
        if (isset($_POST['email']) && isset($_POST['password'])) {
            $form_submitted = true;
            $email = $_POST['email'];
            $password = $_POST['password'];
        }

        if ($form_submitted && $identity->isUserValid($email, $password)) {
            $identity->logIn($email, $password);
            header('location: /front/index');
            exit;
        }

        $this->render('login');
    }

    public function logoutAction()
    {
        $this->app->identity->logOut();
        header('location: /front/index');
        exit;
    }
}