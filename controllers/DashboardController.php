<?php

use Core\Controller;

class DashboardController extends Controller
{
    public function __construct($app)
    {
        parent::__construct($app);
        $this->name = 'dashboard';
    }

    public function indexAction()
    {
        $this->render('index');
    }
}