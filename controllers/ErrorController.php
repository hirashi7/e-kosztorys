<?php

use Core\Controller;

class ErrorController extends Controller
{
    public function __construct(App $app)
    {
        parent::__construct($app);
        $this->name = 'error';
    }

    public function indexAction()
    {
        $this->render('error404');
    }
    public function error404Action(){
        $this->render('error404');
    }

}