<?php

require "vendor/autoload.php";

$app = new App(new DbConnection(), new IdentityService());

$bootstrap = new Bootstrap($app, new Router());
