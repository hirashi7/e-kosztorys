
<!doctype html>
<html lang="<?= $this->app->lang_iso; ?>">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= $this->app->name; ?></title>
    <?= $this->head(); ?>
    <link rel="stylesheet" href="style.css">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="/front/index"><?= $this->app->name; ?></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/front/index">Home</a>
            </li>
        </ul>
        <ul class="navbar-nav mr-0">
            <?php if (!$this->app->identity->isUserLoggedIn()): ?>
                <li class="nav-item active">
                    <a class="nav-link" href="/identity/login">Zaloguj się</a>
                </li>
            <?php else: ?>
                <li class="nav-item active">
                    <a class="nav-link" href="/identity/logout">Wyloguj się</a>
                </li>
            <?php endif; ?>
        </ul>

    </div>
</nav>