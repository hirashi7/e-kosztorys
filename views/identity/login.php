<div class="container">
    <div class="row">
        <div class="col-12 text-center">
            <h1>Logowanie</h1>
        </div>
        <form class="form col-12 col-sm-8 col-md-5 mr-auto ml-auto" method="post" action="">
            <div class="form-group">
                <label for="email" class="form-group d-block">
                    Email:
                    <input type="email" name="email" value="test@test.com" class="form-control">
                </label>
            </div>
            <div class="form-group">
                <label for="password" class="d-block">
                    Hasło:
                    <input type="password" name="password" value="test" class="form-control">
                </label>
            </div>
            <p>
                <button type="submit" class="btn btn-success btn-block">Zaloguj się</button>
            </p>
        </form>
    </div>
</div>