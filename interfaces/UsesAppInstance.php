<?php


interface UsesAppInstance
{
    public function setAppInstance(App $app);
}