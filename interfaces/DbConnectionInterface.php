<?php


interface DbConnectionInterface
{
    function execute(string $command);
}