<?php


interface UsesDatabaseBehavior
{
    function setDatabaseConnection(DbConnectionInterface $db_connection): void;
}