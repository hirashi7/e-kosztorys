<?php


interface RouterInterface
{
    function redirect(string $controller_class, string $action_name, $param = null);

    function redirectHome();

    function redirect404();

    function setAppInstance(App $app);
}