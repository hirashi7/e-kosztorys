<?php


class IdentityService implements IdentityServiceInterface, UsesDatabaseBehavior
{
    const SESSION_STATUS_ACTIVE = 1;
    const SESSION_STATUS_INACTIVE = 0;
    private const _HASHING_OPTIONS = [
        'cost' => 11,
        //'salt' => '837253vb98075360857346058O(*&^)*&^)&(B^)^%_V%B&(_'
    ];
    private $_db_connection;


    function isUserValid(string $email, string $password): bool
    {
        $db = $this->_db_connection;
        $result = $db->execute("SELECT `password` FROM `user` WHERE `email` = '{$email}';");
        if ($result === false) {
            return false;
        }
        if (!isset($result['password'])) {
            return false;
        }
        $password_hash = $result['password'];

        if (password_needs_rehash($password_hash, PASSWORD_DEFAULT, self::_HASHING_OPTIONS)) {
            $password_hash = password_hash($password, PASSWORD_DEFAULT, self::_HASHING_OPTIONS);
        }
        return password_verify($password, $password_hash);
    }


    function setDatabaseConnection(DbConnectionInterface $db_connection): void
    {
        $this->_db_connection = $db_connection;
    }

    function logIn(string $email, string $password)
    {
        $_SESSION['session_status'] = self::SESSION_STATUS_ACTIVE;
        session_regenerate_id();
    }

    function logOut()
    {
        $_SESSION['session_status'] = self::SESSION_STATUS_INACTIVE;
    }

    function isUserLoggedIn(): bool
    {
        if (!isset($_SESSION['session_status'])) return false;
        return $_SESSION['session_status'] === self::SESSION_STATUS_ACTIVE;
    }
}